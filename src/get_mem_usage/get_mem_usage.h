/*
    SPDX-FileCopyrightText: 2021 Bolshevikov Igor <bolshevikov.igor@gmail.com>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/
#ifndef _GET_MEM_USAGE_H_
#define _GET_MEM_USAGE_H_

#include <iostream>
#include <string>

/**
 * @brief Получение данных о нагрузке на RAM/SWAP
 *
 * Просто получаем данные из meminfo.
 * Сначало необходимо вызвать метод получающий данные, а потом методы
 * возвращающие значения.
 */
class MemUsage
{
public:
    /**
     * @brief Структура для возврата полученных параметров
     */
    struct MeasuredParams
    {
        double usageGb;
        double totalGb;
    };
    
    MemUsage() = default;
    ~MemUsage()= default;
    
    /**
     * @brief Запуск процедуры оценки уровня использования памяти
     *
     * @retval true, если измерение успешно
     */
    bool runMeasuring();
    
    /**
     * @brief Получить отчет об использовании RAM (после измерения)
     *
     * @return структура с резульататми измерений
     */
    MeasuredParams getRamUsage();
    
    /**
     * @brief Получить отчет об использовании SWAP (после измерения)
     *
     * @return структура с резульататми измерений
     */
    MeasuredParams getSwapUsage();
private: /* приватные методы */
    /**
     * @brief Процедура получения значение поля из файла /proc/meminfo
     *
     * @param fieldName имя поля
     *
     * @return В случае успеха возвращается значение поля. Или -1 в случае
     *         ошибки.
     */
    ssize_t _getFieldFromMeminfo(
                    const std::string&  fieldName
                    );
    
    /**
     * @brief Логгирование ошибки поиска поля в файле meminfo
     *
     * @param fieldName имя поля которе не получилось найти
     */
    void loggErrFindField(const char* fieldName)
    {
        std::cerr << "MemUsage err: Ошибка поиска поля "
                  << fieldName
                  << std::endl;
    }

private: /* приватные поля */
    /** путевое имя файла meminfo */
    const std::string   _meminfoPname = "/proc/meminfo";
    /** делитель для перевод полученного из meminfo значения в Gb */
    const double        _memGbDiv = 1e6;
    /** полученное общее количество RAM в ГБ */
    double              _ramTotalGb;
    /** полученное количество свободной RAM в ГБ */
    double              _ramFreeGb;
    /** полученное общее количество памяти SWAP раздела в ГБ */
    double              _swapTotalGb;
    /** полученное количество свободной памяти SWAP раздела в ГБ */
    double              _swapFreeGb;
};

#endif /* _GET_MEM_USAGE_H_ */
