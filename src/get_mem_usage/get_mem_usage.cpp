/*
    SPDX-FileCopyrightText: 2021 Bolshevikov Igor <bolshevikov.igor@gmail.com>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/
#include <iostream>
#include <fstream>

/* local includes */
#include "get_mem_usage.h"

/******************************************************************************/
ssize_t MemUsage::_getFieldFromMeminfo(
                                const std::string&  fieldName
                                )
{
    std::ifstream meminfo(_meminfoPname);
    if( ! meminfo.is_open())
    {
        return -1;
    }
    std::string sscanfPattern =   fieldName
                                + ": %ld kB";
    ssize_t readedVal = -1;
    for(std::string line; std::getline(meminfo, line) ;)
    {
        if(line.find(fieldName) == std::string::npos)
        {
            continue;
        }
        auto rc = std::sscanf(
              line.c_str()
            , sscanfPattern.c_str()
            , &readedVal
            );
        if(rc)
        {
            break;
        }
    }
    return readedVal;
}

/******************************************************************************/
bool MemUsage::runMeasuring()
{
    static const char* memAvailField = "MemAvailable";
    auto ret = _getFieldFromMeminfo(memAvailField);
    if(ret < 0)
    {
        loggErrFindField(memAvailField);
        return false;
    }
    _ramFreeGb = (double)ret / _memGbDiv;
    
    
    static const char* memTotalField = "MemTotal";
    ret = _getFieldFromMeminfo(memTotalField);
    if(ret < 0)
    {
        loggErrFindField(memTotalField);
        return false;
    }
    _ramTotalGb = (double)ret / _memGbDiv;
    
    
    static const char* swapFreeField = "SwapFree";
    ret = _getFieldFromMeminfo(swapFreeField);
    if(ret < 0)
    {
        loggErrFindField(swapFreeField);
        return false;
    }
    _swapFreeGb = (double)ret / _memGbDiv;
    
    
    static const char* swapTotalField = "SwapTotal";
    ret = _getFieldFromMeminfo(swapTotalField);
    if(ret < 0)
    {
        loggErrFindField(swapTotalField);
        return false;
    }
    _swapTotalGb = (double)ret / _memGbDiv;
    return true;
}

/******************************************************************************/
MemUsage::MeasuredParams MemUsage::getRamUsage()
{
    MemUsage::MeasuredParams param;
    param.totalGb = _ramTotalGb;
    param.usageGb =   _ramTotalGb
                    - _ramFreeGb;
    return param;
}
/******************************************************************************/
MemUsage::MeasuredParams MemUsage::getSwapUsage()
{
    MemUsage::MeasuredParams param;
    param.totalGb = _swapTotalGb;
    param.usageGb =   _swapTotalGb
                    - _swapFreeGb;
    return param;
}
