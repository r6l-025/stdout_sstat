/*
    SPDX-FileCopyrightText: 2021 Bolshevikov Igor <bolshevikov.igor@gmail.com>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include <iostream>
#include <unistd.h>

#include "result_printer.h"
int main()
{
    ResultPrinter printer;
    
    std::string str;
    printer.printCpuStat(&str);
    std::cout << str << std::endl;
    printer.printDiskStat(&str);
    std::cout << str << std::endl;
    printer.printRamStat(&str);
    std::cout << str << std::endl;
    printer.setUsedNetDevName("eth0");
    for(int i = 0; i < 50; ++i)
    {
        sleep(1);
        std::vector<std::string> strs;
        printer.runNetActivityMeasuring();
        printer.printNetDevStatsRxTx(&strs);
        for(auto& netStr : strs)
        {
            std::cout << netStr << std::endl;
        }
    }
    return (0);
}
