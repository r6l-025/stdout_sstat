/*
    SPDX-FileCopyrightText: 2021 Bolshevikov Igor <bolshevikov.igor@gmail.com>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/
#include <iostream>
#include <sys/statvfs.h>
#include <cstring>
#include <fstream>
#include <filesystem>
#include <list>
namespace fs = std::filesystem;


/* local includes */
#include "get_disk_usage.h"

/******************************************************************************/
bool DiskUsage::getUsage(
                    std::vector<DiskUsage::DiskInfo*>* readedParams
                    )
{
    bool ret = _getMountParams(readedParams);
    if(! ret)
    {
        return ret;
    }
    
    for(auto& readParam : *readedParams)
    {
        ret = _getStatVfs(readParam);
        if( ! ret)
        {
            return ret;
        }
        _rescaleStat(readParam);
    }
    
    return true;
}

/******************************************************************************/
bool DiskUsage::_getStatVfs(
                    DiskInfo* param
                    )
{
    struct statvfs stat;
    int ret = statvfs(
                      param->mountName.c_str()
                    , &stat
                    );
    if(ret < 0)
    {
        std::cerr << "Для пути "
                  << param->mountName.c_str()
                  << " произошла ошибка statvfs: "
                  << strerror(errno)
                  << std::endl;
        return false;
    }
    param->totalSpace = (double)(  stat.f_bsize
                                 * stat.f_blocks
                                );
    auto freeSpace = stat.f_bsize * stat.f_bfree;
    param->usageSpace =   param->totalSpace
                        - (double)freeSpace;
    param->freeSpace = (double)freeSpace;
    return true;
}

/******************************************************************************/
void DiskUsage::_rescaleStat(
                DiskInfo* param
                )
{
    std::vector<std::string> unitNames = {"kB", "MB", "GB", "TB"};
    param->unitName = "B";
    double testVal;
    for(size_t i = 0; i < unitNames.size(); ++i)
    {
        testVal = param->totalSpace / 1024;
        if(testVal < 1)
        {
            break;
        }
        param->unitName = unitNames[i];
        param->totalSpace /= 1024;
        param->usageSpace /= 1024;
        param->freeSpace  /= 1024;
    }
}
/******************************************************************************/
std::tuple<bool, std::string> DiskUsage::_getBlkNameForMapperDev(
                    char* diskName
                    )
{
    std::string resultBlkName;
    std::string sysfsDmDir = "/sys/class/block/";
    const char* realp = fs::read_symlink(diskName).c_str();
    if (NULL == realp)
    {
        std::cerr << "Ошибка разыменовывания ссылки " 
                  << diskName
                  << std::endl;
        return std::make_tuple(false, resultBlkName);
    }

    char* dmBlk = basename(const_cast<char*>(realp));
    if (NULL == dmBlk)
    {
        std::cout << "Ошибка получения базового имени "
                  << dmBlk
                  << std::endl;
        return std::make_tuple(false, resultBlkName);
    }

    sysfsDmDir.append(dmBlk);

    if ( ! fs::exists(sysfsDmDir))
    {
        return std::make_tuple(false, resultBlkName);
    }

    sysfsDmDir.append("/slaves");

    auto sysfsDmDirIt = fs::directory_iterator(sysfsDmDir);
    
    char* blkName = basename(
                        const_cast<char*>(sysfsDmDirIt->path().c_str())
                        );
    resultBlkName = blkName;
    return std::make_tuple(true, resultBlkName);
}

/******************************************************************************/
bool DiskUsage::_getMountParams(
                    std::vector<DiskUsage::DiskInfo*>* readedParams
                    )
{
    std::ifstream mounts(_mountsPname);
    if( ! mounts.is_open())
    {
        std::cerr << "Ошибка открытия файла "
                  << _mountsPname
                  << ": "
                  << std::strerror(errno)
                  << std::endl;
        return false;
    }
    readedParams->clear();
    
    std::string diskPattern                 = "/dev/";
    std::list<std::string> ignoreDiskList   = {"docker"};
    std::string diskMapperPattern           = "/dev/mapper";
    std::string sysfsDmDir                  = "/sys/class/block/";
    std::string sscanfPattern               = "%ms %ms ";
    for(std::string line; std::getline(mounts, line) ;)
    {
        if(0 != (int)line.find(diskPattern))
        {
            continue;
        }
        bool igonoreThisLine = false;
        for (auto ignorePatt : ignoreDiskList)
        {
            if(std::string::npos != line.find(ignorePatt))
            {
                igonoreThisLine = true;
            }
        }
        if (igonoreThisLine)
        {
            continue;
        }
        auto tmpDiskParam = new DiskUsage::DiskInfo();
        char* diskName  = NULL;
        char* mountName = NULL;
        auto rc = std::sscanf(
              line.c_str()
            , sscanfPattern.c_str()
            , &diskName
            , &mountName
            );
        if(rc != 2)
        {
            free(diskName);
            free(mountName);
            delete tmpDiskParam;
            continue;
        }

        if(0 == (int)line.find(diskMapperPattern))
        {
            std::tuple<bool, std::string> ret = _getBlkNameForMapperDev(diskName);
            if( ! std::get<0>(ret))
            {
                free(diskName);
                free(mountName);
                delete tmpDiskParam;
                return false;
            }
            tmpDiskParam->diskDevPname.append("/dev/");
            tmpDiskParam->diskDevPname.append(std::get<1>(ret));
        }
        else
        {
            tmpDiskParam->diskDevPname.append(diskName);
        }

        tmpDiskParam->mountName.append(mountName);
        free(diskName);
        free(mountName);
    
        readedParams->push_back(
                        tmpDiskParam
                        );
    }
    return( ! readedParams->empty());
}

