/*
    SPDX-FileCopyrightText: 2021 Bolshevikov Igor <bolshevikov.igor@gmail.com>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/
#ifndef _GET_CPU_USAGE_H_
#define _GET_CPU_USAGE_H_

#include <string>
#include <vector>
#include <cstring>
#include <cstdint>

/**
 * @brief Класс производящий вычисление нагрузки CPU ядер
 *
 * Алгоритм вычисления нагрузки CPU
 *
 * Из файла /proc/stat достаются значения количества тиков проведенных CPU
 * в том или ином состоянии;
 *
 * Из копии файла /proc/stat, сделанной в прошлый вызов алгоритма, вытаскиваются
 * такие же данные;
 *
 * Из разницы количества тиков в том или ином состоянии процессора к общему
 * количеству тиков прошедших с последнего запуска вычисляется относительная
 * нагрузка на CPU.
 *
 * Создается копия файла /proc/stat для обработки в последующем запуске
 * алгоритма.
 *
 *
 * Способ с копированием файла нужен только из-за предположения что объект
 * класса будет уничтожен и заново создан между измерениями (программа
 * запущена не постоянно, а только по запросу). Если бы объект класса не
 * уничтожался постоянно - можно было бы не создавать копию файла, а просто
 * хранить значения нагрузки для следующего обращения.
 *
 * Еще можно было бы уменьшить выч нагрузку если хранить не копию всего stat
 * файла, а только вычисленные значения нагрузки. Но в этом случае усложнился
 * бы код, т.к. надо было парсить файл со значениями, валидировать их. А так
 * - просто вызываем два раза один и тот же метод разбирающий два разных файла.
 */
class CpuUsage
{
public:
    CpuUsage() = default;
    ~CpuUsage()= default;
    
    bool getCpuUsage(
            std::vector<double>*   cpuUsagePercent
            );
    
private: /* приватные методы */
    struct _CPULoadInfoT;
    /**
     * @brief Извлечение из файла /proc/stat (или его копии) относительного
     *        времени проведенного процессором в разных состояниях с
     *        последнего акта чтения
     *
     * При парсинге из файла stat вытаскиваются значения показателей того
     * сколько (в процентном соотношении) CPU провел вреемни в том или ином
     * состоянии. При вычислении проведенного вермени учитывается предыдущее
     * состояние переданное через cpuInfo.
     *
     * @param cpuInfo   - указатель на вектор из структур с измеренными данными;
     * @param filePName - путевое имя анализируемого файла;
     *
     * @return true, если разбор файла успешен
     */
    bool _runAnalyse(
              std::vector<_CPULoadInfoT>*   cpuInfo
            , const std::string&            filePName
            );
    
    /**
     * @brief Извлечь параметры нагрузки из строки /proc/stat, и пересчитать
     *        промежуточные значения показателей нагрузки
     *
     * Метод получает свежие данные от переданной строки вытащенной из
     * /proc/stat, и на основании сохраненных
     * данных от прошлого чтения вычисляет динамику изменения нагрузки
     * нескольких параметров. Данные на выходе еще не обозначают суммарную
     * нагрузку на CPU, а указывают на процент времени процессора
     * проведенного в том или ином состоянии за прошедший промежуток времени.
     *
     * @param loadInfo  - указатель на структуру в которую будут возвращены
     *                    распарсенные данные из строки /proc/stat
     * @param parseLine - очередная строка /proc/stat подлежащая парсингу
     *
     * @return true, если строка успешно обработана
     */
    bool _updateCPULoad(
              _CPULoadInfoT*                loadInfo
            , const std::string&            parseLine
            );
    
    /**
     * @brief Копирование файла /proc/stat во временный файл
     *
     * @return true, если копирование успешно
     */
    bool _copyCpuStatFile();
    
    /**
     * @brief Логгирование ошибки открытия файла
     *
     * @param fname - путевое имя файла
     */
    void _loggErrOpenFile(
            const std::string               fname
            );
    
    /**
     * @brief Проверка файла на существование
     *
     * @param pName - путевое имя файла
     *
     * @return true, если существует
     */
    bool _fileExist(
            const std::string&              pName
            );
    
    /**
     * @brief Вычислить величину загрузки CPUs по считанным ранее значениям
     *
     * @param cpuUsagePercent - указатель на вектор для возврата вычилсенных
     *                          процентных показателей нагрузки CPU;
     * @param loadInfo        - указатель на вектор с прочитанными из /proc/stat
     *                          значениями;
     */
    void _calcLoadsFromReadedSamples(
              std::vector<double>*              cpuUsagePercent
            , const std::vector<_CPULoadInfoT>* loadInfo
            );

private: /* приватные типы */
    /**
     * @brief Структура для хранения значений вычисленных значений нагрузки
     *        и тиков проведенных процессором в разных состояниях с моменты
     *        запуска OS
     */
    struct _CPULoadInfoT
    {
        /* группа полей с ИЗМЕРЕННОЙ НАГРУЗКОЙ */
        double userLoad;
        double niceLoad;
        double sysLoad;
        double waitLoad;
    
        /* группа полей с СЧИТАННЫМИ ТИКАМИ */
        uint64_t userTicks;
        uint64_t niceTicks;
        uint64_t sysTicks;
        uint64_t idleTicks;
        uint64_t waitTicks;
    } ;
    
private: /* приватные поля */
    const std::string   _cpuStatPname       = "/proc/stat";
    const std::string   _cpuStatCopyPname   = "/var/tmp/stdout_sstat.cpu";
    const size_t        _maxOfCpus          = 64;
};

#endif /* _GET_DISK_USAGE_H_ */
