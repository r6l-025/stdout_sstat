/*
    SPDX-FileCopyrightText: 2021 Bolshevikov Igor <bolshevikov.igor@gmail.com>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/
#include <iostream>
#include <fstream>
#include <regex>
#include <unistd.h>

/* local includes */
#include "get_cpu_usage.h"

/******************************************************************************/
bool CpuUsage::_runAnalyse(
              std::vector<_CPULoadInfoT>*   cpuInfo
            , const std::string&            filePName
            )
{
    std::ifstream cpuStatF(filePName);
    if( ! cpuStatF.is_open())
    {
        std::cerr << "Ошибка открытия файла "
                  << _cpuStatPname
                  << ": "
                  << std::strerror(errno)
                  << std::endl;
        return false;
    }
    
    bool rc = false;
    const auto regexPattern = std::regex("cpu[[:digit:]]");
    std::smatch res;
    size_t cpuLineCnt = 0;
    for(std::string line; std::getline(cpuStatF, line) ;)
    {
        if( ! std::regex_search(line, res, regexPattern))
        {
            continue;
        }
        cpuLineCnt++;
        /* проверяем что не выходим за пределы вектора */
        if(cpuLineCnt >= _maxOfCpus)
        {
            break;
        }
        if(cpuLineCnt > cpuInfo->size())
        {
            cpuInfo->push_back(_CPULoadInfoT{});
        }
        rc = _updateCPULoad(
              &cpuInfo->at(cpuLineCnt - 1) // т.к. ++ до обращения
            , line
            );
    }
    cpuStatF.close();
    return rc;
}
/******************************************************************************/
bool CpuUsage::_updateCPULoad(
                      _CPULoadInfoT*        loadInfo
                    , const std::string&    parseLine
                    )
{
    uint64_t currUserTicks;
    uint64_t currSysTicks;
    uint64_t currNiceTicks;
    uint64_t currIdleTicks;
    uint64_t currWaitTicks;
    uint64_t totalTicks;
    int      cpuId;
    
    int ret = sscanf(
          parseLine.c_str()
        , "cpu%d %lu %lu %lu %lu %lu"
        , &cpuId
        , &currUserTicks
        , &currNiceTicks
        , &currSysTicks
        , &currIdleTicks
        , &currWaitTicks
        );
    if(ret != 6)
    {
        return false;
    }

    totalTicks =   ( currUserTicks - loadInfo->userTicks )
                 + ( currSysTicks  - loadInfo->sysTicks  )
                 + ( currNiceTicks - loadInfo->niceTicks )
                 + ( currIdleTicks - loadInfo->idleTicks )
                 + ( currWaitTicks - loadInfo->waitTicks );

    if ( totalTicks > 10 ) {
        loadInfo->userLoad =
                        (100.0 * (double)(currUserTicks - loadInfo->userTicks))
                      / (double)totalTicks;
        loadInfo->sysLoad  =
                        (100.0 * (double)(currSysTicks  - loadInfo->sysTicks ))
                      / (double)totalTicks;
        loadInfo->niceLoad =
                        (100.0 * (double)(currNiceTicks - loadInfo->niceTicks))
                      / (double)totalTicks;
        loadInfo->waitLoad =
                        (100.0 * (double)(currWaitTicks - loadInfo->waitTicks))
                       / (double)totalTicks;
    }
    else
    {
        loadInfo->userLoad  = 0;
        loadInfo->sysLoad   = 0;
        loadInfo->niceLoad  = 0;
        loadInfo->waitLoad  = 0;
    }

    loadInfo->userTicks = currUserTicks;
    loadInfo->sysTicks  = currSysTicks;
    loadInfo->niceTicks = currNiceTicks;
    loadInfo->idleTicks = currIdleTicks;
    loadInfo->waitTicks = currWaitTicks;
    
    return true;
}

/******************************************************************************/
bool CpuUsage::_copyCpuStatFile()
{
    std::ifstream src(_cpuStatPname);
    std::ofstream dst(_cpuStatCopyPname);
    auto closeFiles = [&]()
    {
        if(src.is_open())
        {
            src.close();
        }
        if(dst.is_open())
        {
            dst.close();
        }
    };
    
    if( ! src.is_open())
    {
        _loggErrOpenFile(_cpuStatPname);
        closeFiles();
        return false;
    }
    
    if( ! dst.is_open())
    {
        _loggErrOpenFile(_cpuStatCopyPname);
        closeFiles();
        return false;
    }
    
    dst << src.rdbuf();
    
    closeFiles();
    return true;
}

/******************************************************************************/
bool CpuUsage::getCpuUsage(
                std::vector<double>*    cpuUsagePercent
                )
{
    std::vector<_CPULoadInfoT> loadInfo;
    
    if( !_fileExist(_cpuStatCopyPname))
    {
        _copyCpuStatFile();
        cpuUsagePercent->clear();
        cpuUsagePercent->push_back(0);
        return true;
    }
    
    auto ret = _runAnalyse(
                  &loadInfo
                , _cpuStatCopyPname
                );
    if( ! ret)
    {
        return false;
    }
    ret = _runAnalyse(
                  &loadInfo
                , _cpuStatPname
                );
    if( ! ret)
    {
        return false;
    }
    
    ret = _copyCpuStatFile();
    if( ! ret)
    {
        return false;
    }
    
    _calcLoadsFromReadedSamples(
                  cpuUsagePercent
                , &loadInfo
                );
    
    return true;
}

/******************************************************************************/
void CpuUsage::_calcLoadsFromReadedSamples(
                          std::vector<double>*              cpuUsagePercent
                        , const std::vector<_CPULoadInfoT>* loadInfo
                        )
{
    cpuUsagePercent->resize(loadInfo->size());
    for(size_t i = 0; i < loadInfo->size(); ++i)
    {
        cpuUsagePercent->at(i) =   loadInfo->at(i).userLoad
                                 + loadInfo->at(i).niceLoad
                                 + loadInfo->at(i).sysLoad
                                 + loadInfo->at(i).waitLoad;
    }
}

/******************************************************************************/
bool CpuUsage::_fileExist(
                const std::string&  pName
                         )
{
    int ret =  access(
                      pName.c_str()
                    , F_OK
                    );
    
    return ret >= 0;
}

/******************************************************************************/
void CpuUsage::_loggErrOpenFile(
                    const std::string   fname
                    )
{
    std::cerr << "Ошибка открытия файла "
              << fname
              << ": "
              << std::strerror(errno)
              << std::endl;
}
