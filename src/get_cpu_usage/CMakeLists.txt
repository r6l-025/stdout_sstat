# SPDX-FileCopyrightText: 2021 Bolshevikov Igor <bolshevikov.igor@gmail.com>
# SPDX-License-Identifier: LGPL-2.1-or-later

###################################################
# локальный cmake
#
# Автоматический поиск исходников и хедеров вынесен в отдельную функцию.
# Если нужно что-то добавить - докидывать в переменные передвавемые
# аргументами
#
###################################################
message ("  --> processing dir: ${CMAKE_CURRENT_SOURCE_DIR}")

auto_search_files_CXX(
    PRJ_FILES_H
    PRJ_FILES_C
    "./"
    )

set (LIB_NAME get_cpu_usage)

add_library (
    ${LIB_NAME}
    ${PRJ_FILES_H}
    ${PRJ_FILES_C}
    )

# линковка и подключчение инклюдников для библиотек проекта
prepare_target(
    ${LIB_NAME}
    )
