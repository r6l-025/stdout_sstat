/*
    SPDX-FileCopyrightText: 2021 Bolshevikov Igor Bolshevikov.igor@gmail.com
    SPDX-License-Identifier: LGPL-2.1-or-later
*/

import QtQuick 2.1
import QtQuick.Layouts 1.1
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.components 2.0 as PlasmaComponents

Item {
    id: mainWindow

    Layout.minimumWidth: 400
    Layout.minimumHeight: 300

    property string textColor: Plasmoid.configuration.textColor
    property string textFont: Plasmoid.configuration.textFont
    property string diskInfoType: Plasmoid.configuration.diskInfoType

    Plasmoid.backgroundHints: Plasmoid.NoBackground

    GridLayout
    {
        columns: 1
        rows: 2
        Layout.fillWidth: false
        Layout.column: 1
        Layout.row: 1

        PlasmaComponents.Label
        {
            Layout.row: 0
            Layout.column: 0
            id: out;
            Layout.alignment: Qt.AlignCenter;
            text: "*";
            color: textColor;
            font.pointSize: 10;
            font.family:textFont;
        }
        GridLayout
        {
            Layout.row: 1
            PlasmaComponents.Label
            {
                Layout.column: 0
                id: netLablelRx;
                Layout.alignment: Qt.AlignLeft;
                text: "_";
                color: textColor;
                font.pointSize: 10;
                font.family:textFont;
            }

            Rectangle
            {
                Layout.column: 1
                width: 20;
                height: 20;
                opacity: 0
            }

            PlasmaComponents.Label
            {
                Layout.column: 2
                id: netLablelTx;
                Layout.alignment: Qt.AlignRight;
                text: "_";
                color: textColor;
                font.pointSize: 10;
                font.family:textFont;
            }
        }
    }

    Timer
    {
        interval: 1000 * plasmoid.configuration.updateInterval;
        running: true;
        repeat: true;
        onTriggered: {
            out.text =   plasmoid.nativeInterface.cpuStat
                       + plasmoid.nativeInterface.diskStat
                       + plasmoid.nativeInterface.ramStat;

            
            var tmp = plasmoid.nativeInterface.netStatsRunMeasuring
            var netStatList = plasmoid.nativeInterface.netStatsRxTx
            netLablelRx.text = netStatList[0];
            netLablelTx.text = netStatList[1];
        }
    }

    Component.onCompleted: {
        plasmoid.nativeInterface.ramInfoType = plasmoid.configuration.diskInfoType
        plasmoid.nativeInterface.sectedNetDevName = plasmoid.configuration.netInterfaceName
    }

}
