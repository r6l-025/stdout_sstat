/*
 *  Copyright 2015 Robert <robspamm@fastmail.fm>
 *  Copyright 2015 David Edmundson <davidedmundson@kde.org>
 *  Copyright 2016 Anselmo L. S. Melo <anselmolsm@gmail.com>
 *  Copyright 2016 Ilya Ostapenko <ostapenko.public@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

Item {
    id: appearancePage

    signal configurationChanged


    property string cfg_textFont
    property string cfg_diskInfoType
    property string cfg_netInterfaceName
    property alias cfg_textColor: colorPicker.chosenColor
    property alias cfg_updateInterval: updateIntervalSpinBox.value

    /***************************************************************************
     * Choose font
     **************************************************************************/
    onCfg_textFontChanged:
    {
        if (cfg_textFont)
        {
            for (var i = 0, j = fontsModel.count; i < j; ++i)
            {
                if (fontsModel.get(i).value == cfg_textFont)
                {
                    fontFamilyComboBox.currentIndex = i
                    break
                }
            }
        }
    }

    ListModel
    {
        id: fontsModel
        Component.onCompleted:
        {
            var arr = [] // use temp array to avoid constant binding stuff
            arr.push({text: i18nc("Use default font", "Default"), value: ""})

            var fonts = Qt.fontFamilies()
            var foundIndex = 0
            for (var i = 0, j = fonts.length; i < j; ++i)
            {
                arr.push({text: fonts[i], value: fonts[i]})
            }
            append(arr)
        }
    }

    /***************************************************************************
     * Choose type of disk usage stat
     **************************************************************************/
    onCfg_diskInfoTypeChanged:
    {
        if (cfg_diskInfoType)
        {
            for (var i = 0, j = diskInfoTypeModel.count; i < j; ++i)
            {
                if (diskInfoTypeModel.get(i).value == cfg_diskInfoType)
                {
                    diskInfoTypeComboBox.currentIndex = i
                    break
                }
            }
        }
    }

    ListModel
    {
        id: diskInfoTypeModel
        Component.onCompleted:
        {
            var arr = []
            arr.push({text: "usage space", value: "usage"})
            arr.push({text: "free space", value: "free"})

            append(arr)
        }
    }

    /***************************************************************************
     * Choose net iface name
     **************************************************************************/
    onCfg_netInterfaceNameChanged:
    {
        if (cfg_netInterfaceName)
        {
            for (var i = 0, j = netInterfaceNameModel.count; i < j; ++i)
            {
                if (netInterfaceNameModel.get(i).text == cfg_netInterfaceName)
                {
                    netInterfaceNameComboBox.currentIndex = i
                    break
                }
            }
        }
    }

    ListModel
    {
        id: netInterfaceNameModel
        Component.onCompleted:
        {
            var arr = []

            var devList = plasmoid.nativeInterface.netDevNames
            for(var i = 0; i < devList.length; ++i)
            {
                arr.push({text: devList[i]})
                console.log(">>>> devList.i = ", devList[i]);
            }

            append(arr)
        }
    }

    /***************************************************************************
     * Layout
     **************************************************************************/
    GridLayout
    { // there's no FormLayout :(
        columns: 2
        Layout.fillWidth: false

        /***************  font conf *******************************************/
        Label
        {
            Layout.fillWidth: false
            horizontalAlignment: Text.AlignRight
            text: i18n("Font:")
        }

        ComboBox {
            id: fontFamilyComboBox
            Layout.fillWidth: true
            // ComboBox's sizing is just utterly broken
            Layout.minimumWidth: units.gridUnit * 10
            model: fontsModel
            // doesn't autodeduce from model because we manually populate it
            textRole: "text"

            onCurrentIndexChanged: {
                var current = model.get(currentIndex)
                if (current) {
                    cfg_textFont = current.value
                    appearancePage.configurationChanged()
                }
            }

        }

        /***************  Disk usage conf *************************************/
        Label
        {
            Layout.fillWidth: false
            horizontalAlignment: Text.AlignRight
            text: i18n("Disk usage info type:")
        }

        ComboBox {
            id: diskInfoTypeComboBox
            Layout.fillWidth: true
            Layout.minimumWidth: units.gridUnit * 10
            model: diskInfoTypeModel
            textRole: "text"

            onCurrentIndexChanged:
            {
                var current = model.get(currentIndex)
                if (current)
                {
                    cfg_diskInfoType = current.value
                    plasmoid.nativeInterface.ramInfoType = current.value
                    appearancePage.configurationChanged()
                }
            }
        }

        /***************  net interface conf **********************************/
        Label
        {
            Layout.fillWidth: false
            horizontalAlignment: Text.AlignRight
            text: i18n("Net interface name:")
        }

        ComboBox {
            id: netInterfaceNameComboBox
            Layout.fillWidth: true
            Layout.minimumWidth: units.gridUnit * 10
            model: netInterfaceNameModel
            textRole: "text"

            onCurrentIndexChanged:
            {
                var current = model.get(currentIndex)
                if (current)
                {
                    cfg_netInterfaceName = current.text
                    plasmoid.nativeInterface.sectedNetDevName = current.text
                    appearancePage.configurationChanged()
                }
            }
        }

        /***************  color conf ******************************************/
        Label
        {
            text: "Color:"
        }
    
        ColorPicker{
            id: colorPicker
        }
        
        /***************  update interval conf ********************************/
        Label {
            text: i18n("Update interval:")
        }

        SpinBox {
            id: updateIntervalSpinBox
            decimals: 1
            stepSize: 0.1
            minimumValue: 0.1
            maximumValue : 3600
            suffix: i18nc("Abbreviation for seconds", "s")
        }
          
        Item {Layout.fillHeight: true}
    }
}
