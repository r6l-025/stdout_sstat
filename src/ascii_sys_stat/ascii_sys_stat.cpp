/*
    SPDX-FileCopyrightText: 2021 Bolshevikov Igor <bolshevikov.igor@gmail.com>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include <KLocalizedString>

#include "ascii_sys_stat.h"
#include "result_printer.h"

/******************************************************************************/
AsciiSysStat::AsciiSysStat(
                      QObject*              parent
                    , const QVariantList&   args
                    ) : Plasma::Applet(
                                  parent
                                , args
                                )
{
    _statMainer = new ResultPrinter();
}

/******************************************************************************/
AsciiSysStat::~AsciiSysStat()
{
}

/******************************************************************************/
QString AsciiSysStat::getCpuStat() const
{
    std::string str;
    _statMainer->printCpuStat(&str);
    return QString(str.c_str());
}

/******************************************************************************/
QString AsciiSysStat::getRamStat() const
{
    std::string str;
    _statMainer->printRamStat(&str);
    return QString(str.c_str());
}

/******************************************************************************/
QString AsciiSysStat::getDiskStat() const
{
    std::string str;
    _statMainer->printDiskStat(&str);
    return QString(str.c_str());
}

/******************************************************************************/
int AsciiSysStat::startStatsRunMeasuring() const
{
    _statMainer->runNetActivityMeasuring();
    return 1;
}

/******************************************************************************/
QStringList AsciiSysStat::getNetStatsRxTx() const
{
    std::vector<std::string> strings;
    _statMainer->printNetDevStatsRxTx(&strings);
    QStringList strList;
    for(auto& string : strings)
    {
        strList.push_back(QString(string.c_str()));
    }
    return strList;
}

/******************************************************************************/
QString AsciiSysStat::getDiskInfoType()
{
    return _currentDiskInfoType;
}

/******************************************************************************/
void AsciiSysStat::setDiskInfoType(QString type)
{
    if(type.contains("usage"))
    {
        _statMainer->setDiskInfoType(
                            ResultPrinter::DiskUsageStatTypeT::UsageIfno
                            );
        _currentDiskInfoType = "usage";
    } else
    {
        _statMainer->setDiskInfoType(
                            ResultPrinter::DiskUsageStatTypeT::FreeInfo
                            );
        _currentDiskInfoType = "free";
    }
    
}

/******************************************************************************/
QStringList AsciiSysStat::getNetDevNames() const
{
    std::vector<std::string> strs;
    _statMainer->getNetDevicesNames(&strs);
    
    QStringList   devNameList;
    for(auto& str : strs)
    {
        devNameList.append(QString(str.c_str()));
    }
    
    return devNameList;
}

/******************************************************************************/
QString AsciiSysStat::getSectedNetDevName()
{
    return _currentNetDevName;
}

/******************************************************************************/
void AsciiSysStat::setSectedNetDevName(QString devName)
{
    _currentNetDevName = devName;
    _statMainer->setUsedNetDevName(devName.toStdString());
}

K_EXPORT_PLASMA_APPLET_WITH_JSON(AsciiSysStat, AsciiSysStat, "metadata.json")

#include "ascii_sys_stat.moc"
