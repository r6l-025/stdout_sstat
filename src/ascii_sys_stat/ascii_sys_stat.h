/*
    SPDX-FileCopyrightText: 2021 Bolshevikov Igor <bolshevikov.igor@gmail.com>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/
#ifndef _STDOUT_SSTAT_H_
#define _STDOUT_SSTAT_H_

#include <Plasma/Applet>
#include <QStringList>

class ResultPrinter;

class AsciiSysStat : public Plasma::Applet
{
    Q_OBJECT
    Q_PROPERTY(
            QString     cpuStat
            READ        getCpuStat CONSTANT
            )
    Q_PROPERTY(
            QString     ramStat
            READ        getRamStat CONSTANT
            )
    Q_PROPERTY(
            QString     diskStat
            READ        getDiskStat CONSTANT
            )
    Q_PROPERTY(
            QStringList netStatsRxTx
            READ        getNetStatsRxTx CONSTANT
            )
    Q_PROPERTY(
            int netStatsRunMeasuring
            READ        startStatsRunMeasuring CONSTANT
            )
    Q_PROPERTY(
            QString     ramInfoType
            READ        getDiskInfoType
            WRITE       setDiskInfoType
            )
    Q_PROPERTY(
            QStringList netDevNames
            READ        getNetDevNames CONSTANT
            )
    Q_PROPERTY(
            QString     sectedNetDevName
            READ        getSectedNetDevName
            WRITE       setSectedNetDevName
            )

public: /* публичные методы */
    AsciiSysStat(
              QObject*              parent
            , const QVariantList&   args
                );
    ~AsciiSysStat();

    QString getCpuStat() const;
    QString getRamStat() const;
    QString getDiskStat() const;
    QStringList getNetStatsRxTx() const;
    int startStatsRunMeasuring() const;
    
    
    QStringList getNetDevNames() const;
    QString getSectedNetDevName();
    void setSectedNetDevName(QString);
    
    /**
     * @brief Геттер qml свойства определяющего тип вывода статистики по
     *        использованию дисковой памяти
     *
     * @return строка с текущим типом
     */
    QString getDiskInfoType();
    
    /**
     * @brief Сеттер для qml свойства определяющего тип вывода статистики по
     *        использованию дисковой памяти
     *
     *  @param statType - строка с именем типа
     */
    void setDiskInfoType(QString statType);

private: /* приватные поля */
    /** указатель на объект выдающий форматированную статистику */
    ResultPrinter*      _statMainer;
    /** текущее значение свойства DiskInfoType */
    QString             _currentDiskInfoType;
    /** текущее имя выбранного сетевого интерфейса */
    QString             _currentNetDevName;
};

#endif /* _STDOUT_SSTAT_H_ */
    
