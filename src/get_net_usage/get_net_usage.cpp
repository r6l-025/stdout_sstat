#include <iostream>
#include <fstream>
#include <filesystem>
#include <chrono>
#include <algorithm>

/* local includes */
#include "get_net_usage.h"

/*****************************************************************************/
bool NetUsage::runNetActivityMeasuring()
{
    _lastMeasuredActivities.clear();
    MeasureResult   measure = _runMeasurment();
    
    for(auto& newMeasuredDev : measure)
    {
        DevActivity obj;
        auto it = std::find_if(
                         _lastMeasureResult.begin()
                       , _lastMeasureResult.end()
                       , [&](NetUsage::NetStamp& stamp)
                        {
                            return stamp.devName == newMeasuredDev.devName;
                        }
                       );
        if(it != _lastMeasureResult.end())
        {
            /* если совпадение по именам - вычисляем нагрузку */
            auto timeDelta  =   newMeasuredDev.timeStamp
                              - it->timeStamp;
            if(timeDelta <= 0)
            {
                continue;
            }

            obj.devRxLoad   =   (double)newMeasuredDev.devRxBytes
                              - (double)it->devRxBytes;
            obj.devRxLoad  /= (double)timeDelta;

            obj.devTxLoad   =   (double)newMeasuredDev.devTxBytes
                              - (double)it->devTxBytes;
            obj.devTxLoad  /= (double)timeDelta;
        }
        else
        {
            obj.devRxLoad   = static_cast<double>(newMeasuredDev.devRxBytes);
            obj.devTxLoad   = (double)newMeasuredDev.devTxBytes;
        }
        obj.devName     = newMeasuredDev.devName;
    
        _lastMeasuredActivities.push_back(std::move(obj));
    }

    _lastMeasureResult = measure;

    return (! _lastMeasuredActivities.empty());
}

/******************************************************************************/
void NetUsage::_rescaleStat(
                  double*       countOfUnits
                , std::string*  unitName
                           )
{
    std::vector<std::string> unitNames = {"kB/S", "MB/S", "GB/S"};
    *unitName = "B/S";
    double testVal;
    for(auto & i : unitNames)
    {
        testVal = *countOfUnits / 1024;
        if(testVal < 1)
        {
            break;
        }
        *unitName = i;
        *countOfUnits /= 1024;
    }
}


/*****************************************************************************/
NetUsage::MeasureResult NetUsage::_runMeasurment()
{
    MeasureResult res;
    res.clear();

    for(auto& p: std::filesystem::directory_iterator(_netDevPathPrefux))
    {
        NetStamp tmp;

        std::string path = p.path();
        ssize_t rxBytes = _getBytesFromFile(path + _netDevRxStatPathSuffix);
        if(rxBytes < 0)
        {
            continue;
        }

        ssize_t txBytes = _getBytesFromFile(path + _netDevTxStatPathSuffix);
        if(txBytes < 0)
        {
            continue;
        }

        auto time = (uint64_t)std::chrono::duration_cast<std::chrono::seconds>(
                           std::chrono::steady_clock::now().time_since_epoch()
                           ).count();

        tmp.devRxBytes  = (size_t)rxBytes;
        tmp.devTxBytes  = (size_t)txBytes;
        tmp.devName     = p.path().filename();
        tmp.timeStamp   = time;

        res.push_back(std::move(tmp));
    }

    return res;
}

/*****************************************************************************/
ssize_t NetUsage::_getBytesFromFile(
                            const std::string&  pname
                                   )
{
    std::ifstream bytesFile(pname);
    if( ! bytesFile.is_open())
    {
        return -1;
    }
    std::string line;
    std::getline(bytesFile, line);
    ssize_t val = 0;
    auto rc = std::sscanf(
                      line.c_str()
                    , "%ld"
                    , &val
                    );
    if(rc > 0)
    {
        return val;
    }

    return -1;
}

/*****************************************************************************/
NetUsage::StrArr NetUsage::getNetDevNames()
{
    NetUsage::StrArr strs;
    
    for(auto& dev : _lastMeasureResult)
    {
        strs.push_back(dev.devName);
    }
    return strs;
}

/*****************************************************************************/
bool NetUsage::getNetActivityForDev(
                          const std::string&    devName
                        , DevActivity*          devActivity
                        )
{
    auto it = std::find_if(
                      _lastMeasuredActivities.begin()
                    , _lastMeasuredActivities.end()
                    , [&](NetUsage::DevActivity& _devActivity)
                    {
                        return _devActivity.devName == devName;
                    }
                );
    
    if(it != _lastMeasuredActivities.end())
    {
        *devActivity = *it;
        return true;
    }
    return false;
}
