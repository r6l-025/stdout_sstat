#ifndef _GET_NET_USAGE_H_
#define _GET_NET_USAGE_H_

#include <vector>
#include <string>

/*
 * Сценарий использования:
 *
 *  - плазмоид запрашивает у объекта список имен;
 *  - пользователь выбирает имя;
 *  - плазмоид запрашивает статистику по имени
 *      - если имени уже нет - то возвращаем отрицательное значение;
 *  - если плазмои получил отрицательное значение:
 *      - обновляет список имен, и запрашивает данные для первого из списка;
 */

class NetUsage
{
public: /* публичные типы */
    typedef std::vector<std::string>           StrArr;
    /**
     * @brief структура для хранения итоговой статистики по нагруженности
     *        устройства пердачи данных по сети
     */
    struct DevActivity
    {
        /** имя сетевого интерфейса */
        std::string     devName;
        /** Скорость приема данных */
        double          devRxLoad;
        /** Единицы измерения скорости приема */
        std::string     devRxUnitNames;
        /** Скорость передачи данных */
        double          devTxLoad;
        /** Единицы измерения скорости передачи */
        std::string     devTxUnitNames;
    };
    /**
     * @brief суммарная статистика по всем устройствам
     */
    typedef std::vector<NetUsage::DevActivity> NetsActivity;

private: /* приватные типы */
    /**
     * @brief Структура замера статистики устройства (сырые данные)
     */
    struct NetStamp
    {
        /** имя сетевого интерфейса */
        std::string     devName;
        /** количество принятых устройством байт */
        size_t          devRxBytes;
        /** количество переданых устройством байт */
        size_t          devTxBytes;
        /** временная метка замера - кол-во секунд с начала эпохи */
        size_t          timeStamp;
    };
    /**
     * @brief Сырые замеры статистики по всем устройствам
     */
    typedef std::vector<NetUsage::NetStamp>    MeasureResult;

public: /* публичные методы */
    NetUsage() = default;
    /**
     * @brief Получить массив строк с именами сетевых интерфейсов
     *
     * @retval массив строк с именами интерфейсов
     */
    StrArr getNetDevNames();

    /**
     * @brief Запустить обнаружение и сбор статистики по сетевым интерфейсам
     *
     * @return true, если хотя бы одно устройство обнаружено
     */
    bool runNetActivityMeasuring();
    
    /**
     * @brief Получить статистику по акитвности устройства (по его имени)
     *
     * @param devName строка с именем утстройства
     *
     * @return структура с отчетом о срежней нагрузке устройства за время с
     *         последнего измерения
     */
    bool getNetActivityForDev(
                      const std::string&    devName
                    , DevActivity*          devActivity
                    );

private: /* приватные методы */
    /**
     * @brief Получить значение количества байт из файла специального вида
     *
     * @param pname - путевое имя файла
     *
     * @retval прочитанное значение
     */
    ssize_t _getBytesFromFile(const std::string& pname);

    /**
     * @brief Снять измерения
     */
    NetUsage::MeasureResult _runMeasurment();

    void _rescaleStat(
                  double*       countOfUnits
                , std::string*  unitName
                );

private: /* приватные полля */
    /** имя директории сдержащей поддиректории с сетевыми устройствами  */
    const std::string       _netDevPathPrefux = "/sys/class/net";
    /** суффикс пути к файлу со статистикой по принятым устройством данным */
    const std::string       _netDevRxStatPathSuffix = "/statistics/rx_bytes";
    /** суффикс пути к файлу со статистикой по переданным устройством данным */
    const std::string       _netDevTxStatPathSuffix = "/statistics/tx_bytes";
    
    NetsActivity            _lastMeasuredActivities;
    MeasureResult           _lastMeasureResult;
};

 
 

#endif /* _GET_NET_USAGE_H_ */
