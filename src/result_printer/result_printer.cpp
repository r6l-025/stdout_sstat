#include <iostream>
#include <fstream>
#include <cmath>
#include <iomanip>

/* local includes */
#include "result_printer.h"

/******************************************************************************/
ResultPrinter::ResultPrinter()
{
    _netUsage.runNetActivityMeasuring();
}

/******************************************************************************/
void ResultPrinter::setDiskInfoType(const DiskUsageStatTypeT& statType)
{
    _diskStatType = statType;
}

/******************************************************************************/
void ResultPrinter::setUsedNetDevName(
                            const std::string& devName
                            )
{
    _selectednNetDevName = devName;
}

/******************************************************************************/
void ResultPrinter::getNetDevicesNames(
                            std::vector<std::string>* devicesNames
                            )
{
    _netUsage.runNetActivityMeasuring();
    *devicesNames = _netUsage.getNetDevNames();
}

/******************************************************************************/
bool ResultPrinter::_getMemUsage()
{
    MemUsage memUsage;
    bool ret = memUsage.runMeasuring();
    if(! ret)
    {
        return false;
    }
    _measuredRamUsage  = memUsage.getRamUsage();
    _measuredSwapUsage = memUsage.getSwapUsage();
    
    return true;
}

/******************************************************************************/
bool ResultPrinter::_getDiskUsage()
{
    DiskUsage diskUsage;
    return diskUsage.getUsage(&_measuredDiskUsage);
}

/******************************************************************************/
bool ResultPrinter::_getCpuUsage()
{
    CpuUsage cpuUsage;
    return cpuUsage.getCpuUsage(&_measuredCpuUsage);
}

/******************************************************************************/
void ResultPrinter::_makeCpuUsageFirstColumnString(
                                            std::vector<std::string>* strings
                                            )
{
    for(size_t i = 0; i < _measuredCpuUsage.size(); ++i)
    {
        std::string string;
        string = "cpu " + std::to_string(i);
        if(string.size() < _maxFirstColumnLen)
        {
            size_t emptySuffLen =   _maxFirstColumnLen
                                  - string.size();
            string += std::string(
                              emptySuffLen
                            , ' '
                            );
        }
        strings->push_back(std::move(string));
    }
}

/******************************************************************************/
void ResultPrinter::_makeCpuUsageSecondColumnString(
                                            std::vector<std::string>* strings
                                            )
{
    for(double i : _measuredCpuUsage)
    {
        std::string percentStr;
        _makeProgressBarString(
                      i
                    , _percentStringLen
                    , &percentStr
                    );
        std::string cpuUsageStr;
        cpuUsageStr += percentStr;
        cpuUsageStr += " ";
        cpuUsageStr += _doubleTostring(i);
        cpuUsageStr += "%";
        strings->push_back(std::move(cpuUsageStr));
    }
}

/******************************************************************************/
void ResultPrinter::_makeCpuUsageString(
                                std::string* string
                                )
{
    std::vector<std::string> cpuUsageFirstColumnStrings;
    _makeCpuUsageFirstColumnString(
                            &cpuUsageFirstColumnStrings
                            );
    std::vector<std::string> cpuUsageSecondColumnStrings;
    _makeCpuUsageSecondColumnString(
                            &cpuUsageSecondColumnStrings
                            );
    *string = "CPU Usage\n\n";
    for(size_t i = 0; i < cpuUsageFirstColumnStrings.size(); ++i)
    {
        *string += cpuUsageFirstColumnStrings[i];
        *string += cpuUsageSecondColumnStrings[i];
        *string += "\n";
    }
}

/******************************************************************************/
void ResultPrinter::_makeDiskUsageFirstColumnString(
                                            std::vector<std::string>* strings
                                            )
{
    size_t devsNamesMaxStrLen = 0;
    size_t devsSizesMaxStrLen = 0;
    for(auto& diskParam : _measuredDiskUsage)
    {
        if(diskParam->diskDevPname.size() > devsNamesMaxStrLen)
        {
            devsNamesMaxStrLen = diskParam->diskDevPname.size();
        }
        if(_getDiskStatString(diskParam).size() > devsSizesMaxStrLen)
        {
            devsSizesMaxStrLen = _getDiskStatString(diskParam).size();
        }
    }
    
    for(auto& diskParam : _measuredDiskUsage)
    {
        std::string string;
        string  = diskParam->diskDevPname;
        string += std::string(
                             devsNamesMaxStrLen
                          -  diskParam->diskDevPname.size()
                        , ' '
                        );
        string += ' ';
        auto sizeStr = _getDiskStatString(diskParam);
        sizeStr += std::string(
                            devsSizesMaxStrLen
                          - _getDiskStatString(diskParam).size()
                        , ' '
                        );
        string += sizeStr;
        string += " / ";
        string += _doubleTostring(diskParam->totalSpace);
        string += " ";
        string += diskParam->unitName;
        
        if(string.size() < _maxFirstColumnLen)
        {
            size_t emptySuffLen =   _maxFirstColumnLen
                                  - string.size();
            string += std::string(
                              emptySuffLen
                            , ' '
                            );
        }
        strings->push_back(std::move(string));
    }
}

/******************************************************************************/
std::string ResultPrinter::_getDiskStatString(DiskUsage::DiskInfo* diskParam)
{
    switch(_diskStatType)
    {
    case DiskUsageStatTypeT::FreeInfo:
        return _doubleTostring(diskParam->freeSpace);
    default:
        return _doubleTostring(diskParam->usageSpace);
    }
}

/******************************************************************************/
void ResultPrinter::_makeDiskUsageSecondColumnString(
                                            std::vector<std::string>* strings
                                            )
{
    for(auto& diskInfo : _measuredDiskUsage)
    {
        double percent =   diskInfo->usageSpace
                         / diskInfo->totalSpace
                         * 100.0;
        std::string percentStr;
        _makeProgressBarString(
                          percent
                        , _percentStringLen
                        , &percentStr
                        );
        std::string cpuUsageStr;
        cpuUsageStr += percentStr;
        cpuUsageStr += _makeDiskUsageSecondColumnPercents(percent);
        cpuUsageStr += diskInfo->mountName;
        strings->push_back(std::move(cpuUsageStr));
    }
}

/******************************************************************************/
std::string ResultPrinter::_makeDiskUsageSecondColumnPercents(
                                                double percent
                                                )
{
    const size_t percentStrMaxLen = 7;
    auto string = " " + _doubleTostring(percent);
    string += "%";
    if(string.size() < percentStrMaxLen)
    {
        string += std::string(percentStrMaxLen - string.size(), ' ');
    }
    string += "  ";
    return string;
}

/******************************************************************************/
void ResultPrinter::_makeDiskUsageString(
                                    std::string* string
                                    )
{
    std::vector<std::string> diskUsageFirstColumnStrings;
    _makeDiskUsageFirstColumnString(
                            &diskUsageFirstColumnStrings
                            );
    std::vector<std::string> diskUsageSecondColumnStrings;
    _makeDiskUsageSecondColumnString(
                            &diskUsageSecondColumnStrings
                            );
    *string += "\n\nDisks Usage\n\n";
    for(size_t i = 0; i < diskUsageFirstColumnStrings.size(); ++i)
    {
        *string += diskUsageFirstColumnStrings[i];
        *string += diskUsageSecondColumnStrings[i];
        *string += "\n";
    }
    
}

/******************************************************************************/
void ResultPrinter::_makeMemUsageFirstColumnString(
                                      std::string*                      string
                                    , const MemUsage::MeasuredParams&   param
                                    )
{
    *string  = _doubleTostring(param.usageGb, 3);
    *string += " / ";
    *string += _doubleTostring(param.totalGb, 3);
    *string += " GB";
    
    if(string->size() < _maxFirstColumnLen)
    {
        size_t emptySuffLen =   _maxFirstColumnLen
                              - string->size();
        *string += std::string(
                          emptySuffLen
                        , ' '
                        );
    }
}

/******************************************************************************/
void ResultPrinter::_makeMemUsageSecondColumnString(
                                        std::string*                    string
                                      , const MemUsage::MeasuredParams& param
                                      )
{
    double percent =   param.usageGb
                     / param.totalGb
                     * 100.0;
    std::string percentStr;
    _makeProgressBarString(
                      percent
                    , _percentStringLen
                    , &percentStr
                    );
    string->append(percentStr);
    string->append(" ");
    string->append(_doubleTostring(percent));
    string->append("%");
    string->append("\n");
}

/******************************************************************************/
void ResultPrinter::_makeRAMUsageString(
                                std::string* string
                                )
{
    std::string ramUsageFirstColumnStrings;
    _makeMemUsageFirstColumnString(
                              &ramUsageFirstColumnStrings
                            , _measuredRamUsage
                            );
    std::string ramUsageSecondColumnStrings;
    _makeMemUsageSecondColumnString(
                              &ramUsageSecondColumnStrings
                            , _measuredRamUsage
                            );
    *string += "\n\nRAM Usage\n\n";
    *string += ramUsageFirstColumnStrings;
    *string += ramUsageSecondColumnStrings;
}

/******************************************************************************/
void ResultPrinter::_makeSWAPUsageString(
                                std::string* string
                                )
{
    std::string swapUsageFirstColumnStrings;
    _makeMemUsageFirstColumnString(
                              &swapUsageFirstColumnStrings
                            , _measuredSwapUsage
                            );
    std::string swapUsageSecondColumnStrings;
    _makeMemUsageSecondColumnString(
                              &swapUsageSecondColumnStrings
                            , _measuredSwapUsage
                            );
    *string += "\n\nSWAP Usage\n\n";
    *string += swapUsageFirstColumnStrings;
    *string += swapUsageSecondColumnStrings;
}

/******************************************************************************/
void ResultPrinter::_makeProgressBarString(
                              double        valPercent
                            , size_t        lenOfFullBar
                            , std::string*  resultStr
                            )
{
    double percentBySymbol = 100.0 / (double)lenOfFullBar;
    auto tmp = valPercent / percentBySymbol;
    auto countOfSymbols = (size_t)round(tmp);
    if(countOfSymbols > lenOfFullBar)
    {
        countOfSymbols = lenOfFullBar;
    }
    
    *resultStr = "[";
    *resultStr += std::string(countOfSymbols, '#');
    *resultStr += std::string(lenOfFullBar - countOfSymbols, '-');
    *resultStr += "]";
}

/******************************************************************************/
bool ResultPrinter::printCpuStat(std::string* str)
{
    bool ret = _getCpuUsage();
    if( ! ret)
    {
        *str = "Error! Can not get Cpu stat";
        return false;
    }
    _makeCpuUsageString(str);
    return ret;
}

/******************************************************************************/
bool ResultPrinter::printRamStat(std::string* str)
{
    bool ret = _getMemUsage();
    if( ! ret)
    {
        *str = "Error! Can not get Ram stat";
        return false;
    }
    _makeRAMUsageString(str);
    _makeSWAPUsageString(str);
    
    return true;
}

/******************************************************************************/
bool ResultPrinter::printDiskStat(std::string* str)
{
    bool ret = _getDiskUsage();
    if( ! ret)
    {
        *str = "Error! Can not get Disk stat";
        return false;
    }
    _makeDiskUsageString(str);
    return ret;
}

/******************************************************************************/
void ResultPrinter::printNetDevStatsRxTx(std::vector<std::string>* strs)
{
    *strs = _netGraphPrintedStrings;
}

/******************************************************************************/
bool ResultPrinter::runNetActivityMeasuring()
{
    _netUsage.runNetActivityMeasuring();

    std::string rxStr;
    std::string txStr;
    _netGraphPrintedStrings.clear();
    
    auto pushFunc = [&]()
    {
        _netGraphPrintedStrings.push_back(rxStr); // Rx
        _netGraphPrintedStrings.push_back(txStr); // Tx
    };
    
    bool ret = _netUsage.getNetActivityForDev(
                          _selectednNetDevName
                        , &_devActivity
                        );
    if( ! ret)
    {
        std::string errStr;
        errStr = "\nError! Can not get Net stat for ";
        errStr += _selectednNetDevName;
        rxStr = errStr;
        txStr = errStr;
        pushFunc();
        return false;
    }
    _netGraphFormerRx.addItem(_devActivity.devRxLoad);
    _netGraphFormerTx.addItem(_devActivity.devTxLoad);
    
    rxStr  = "\n\nNet Load\n\n";
    
    rxStr += _devActivity.devName;
    rxStr += " RX (";
    rxStr += _netGraphFormerRx.getNameOfScaller();
    rxStr +=  "): \n";
    rxStr += _netGraphFormerRx.printGraph();
    /* компенсируем закоголов (выравнивания RX/TX строк по высоте) */
    rxStr += "\n\n\n\n";
    
    txStr += _devActivity.devName;
    txStr += " TX (";
    txStr += _netGraphFormerTx.getNameOfScaller();
    txStr +=  "): \n";
    txStr += _netGraphFormerTx.printGraph();
    
    pushFunc();

    return true;
}

/******************************************************************************/
std::string ResultPrinter::_doubleTostring(
                                      double val
                                    , int    precision
                                    )
{
    std::stringstream tmpStream;
    tmpStream << std::fixed
              << std::setprecision(precision)
              << val;
    return tmpStream.str();
}

