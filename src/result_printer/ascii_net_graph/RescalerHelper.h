#ifndef _RESCALER_HELPER_H_
#define _RESCALER_HELPER_H_

#include <vector>
#include <string>
#include <map>
#include <cmath>

class RescallerHelper
{
public: /* публичные типы */
    enum class Scallers
    {
        B,
        KB,
        MB,
        GB
    };
public: /* публичные методы */
    RescallerHelper();
    /**
     * @brief Получить имя размерности заданного коэфф. масштабирования
     *
     * @param scalCode - код коэффициента масштабирования
     *
     * @retval строка с буквенным кодом размерности масштабирования
     */
    std::string getNameOfScaller(RescallerHelper::Scallers scalCode);

    /**
     * @brief Произвести перемасштабирование содержимого контейнера
     *
     * Метод обходит весь контейнер и изменяет масштаб всех элементов так, чтоб
     * полученная величина была приведена к СИ типам (B/kB/MB/GB), и по
     * возможности лежала в интервале от 0 до 999,(9)
     *
     * Т.е. если максимальная величина равна 1042B, то имеет смысл
     * перемаштабировать все значения в kB, чтоб она стала 1 kB.
     *
     * @param valsArray - указатель на вектор с масштабируемыми элементами
     *
     * @retval true, если масштабирование потребовалось выполнить, false - если
     * со значениями все в порядке, и масштабировать ничего не надо.
     */
    bool scale(
              std::vector<double>*  valsArray
            , double*               newMaxVal
            );
    
    /**
     * @brief Вычислить новый индекс масштабирвоания по предоставленому числу
     *
     * Метод принимает некоторе число, которое подразумевается самым большим в
     * выборке. По этому числу метод подгоняет коэффициент масштабирования так,
     * чтоб это максимальное число лежало в диапазоне [1:1024].
     *
     * @param maxValInBytes - число по которому подгоняем коэффициента
     *                        масштабирвоания
     *
     * @retval индекс масштабироавния (не сам коэффициент, а лишь индекс по
     * поторому можно найти коэффициент
     */
    size_t calcNewScaleIdx(
            double                  maxValInBytes
            ) const;

    /**
     * @brief Пересчитать коэффициент масштабирования по предоставленому
     *        индексу масштабирования
     *
     * Пересчитывает коэффициенты масштабирования из текущего в новый
     *
     * @param newScaleIdx - индекс масштабироавния по которому вычисляется
     *                      коэффициент
     *
     * @retval вычисленный мультипликативный коэффициент масштабироавния
     */
    double calcRescaleFactor(
            size_t                  newScaleIdx
            ) const;
    /**
     * @brief Получить текущий коэффициент масштабирования для буфера
     *
     * @retval мультипликативный коэффицент
     */
    double getCurrentScaleFactor() const;
    
    /**
     * @brief Получить текущий код масштабирования
     *
     * @return элемент перечисления кодов масштабирвоания
     */
    RescallerHelper::Scallers getCurrentScaleCode() const;

private: /* приватные поля */
    /** набор кодов масштабов, и их строковое описание */
    const std::map<RescallerHelper::Scallers, std::string>  _typeNames = 
    {
          {Scallers::B , "B" }
        , {Scallers::KB, "KB"}
        , {Scallers::MB, "MB"}
        , {Scallers::GB, "GB"}
    };

    size_t          _currentScallerIdx;
};

#endif /* _RESCALER_HELPER_H_ */
