#include <iostream>
#include <algorithm>
#include <cassert>
#include <iomanip>

#include "ascii_net_graph.h"

/*****************************************************************************/
AsciiNetGraph::AsciiNetGraph() :
                                 _itemsBufferHead(0)
                               , _maxValInVector(0)
                               , _maxValInVectorIdx(0)
{
    _itemsBuffer.resize(_containerSize);
}

/*****************************************************************************/
void AsciiNetGraph::addItem(double newVal)
{
    /*
     * сразу масштабируем значение в коэффициент используемый для всего
     * буфера
     */
    newVal *= _helper.getCurrentScaleFactor();
    
    _pushItemToCircBuffer(newVal);
    
    _helper.scale(
                &_itemsBuffer
              , &_maxValInVector
              );
}

/*****************************************************************************/
void AsciiNetGraph::_pushItemToCircBuffer(double newItem)
{
    _itemsBuffer[_itemsBufferHead] = newItem;
    _itemsBufferHead++;
    if(_itemsBufferHead == _containerSize)
    {
        _itemsBufferHead = 0;
    }
}

/*****************************************************************************/
void AsciiNetGraph::_getItemsFromCircBuff(
                    std::vector<double>*    items
                    )
{
    /* назначим alias типу итератора контейнера */
    using itType=__gnu_cxx::__normal_iterator<
                                          double*
                                        , std::vector<double>
                                        >::difference_type;
    items->resize(_itemsBuffer.size());
    
    assert((
        _itemsBuffer.begin() + (itType)_itemsBufferHead > _itemsBuffer.end(),
        "Итератор _itemsBuffer вылез за пределы вектора!"
        ));
    std::copy(
          _itemsBuffer.begin() + (itType)_itemsBufferHead
        , _itemsBuffer.end()
        , items->begin()
        );

    /* 
     * проходим "заворот буера", и копируем данные от начала буфера,
     * до индекса head
     */
    if(_itemsBufferHead != 0)
    {
        assert((
                      _itemsBuffer.begin()
                    + (itType)_itemsBufferHead
                  >   items->end()
                    - (itType)_itemsBufferHead
                , "Итератор _itemsBuffer вылез за пределы вектора items!"
                ));
        std::copy(
              _itemsBuffer.begin()
            , _itemsBuffer.begin() + (itType)_itemsBufferHead
            , items->end() - (itType)_itemsBufferHead
            );
    }
}

/*****************************************************************************/
std::string AsciiNetGraph::printGraph()
{
    std::string outStr;
    
    /* шаг квантования по уровню */
    auto levelQuant = _maxValInVector / (double)_leveDiscretsCount;
    
    std::vector<double> outBuff;
    _getItemsFromCircBuff(&outBuff);
    
    for(ssize_t qIdx = (ssize_t)_leveDiscretsCount; qIdx > 0 ; --qIdx)
    {
        outStr += _makeSpeedStr(levelQuant * (double)qIdx);
        outStr += " ";
        for(auto& item : outBuff)
        {
            outStr +=   (    item < (levelQuant * (double)qIdx)
                          || _itemIsZero(item)
                        )
                      ? "."
                      : "|";
        }
        outStr += "\n";
    }
    return outStr;
}

/******************************************************************************/
bool AsciiNetGraph::_itemIsZero(double item)
{
    return item < 0.001;
}

/******************************************************************************/
std::string AsciiNetGraph::_makeSpeedStr(
                                  double    val
                                , size_t    precision
                                )
{

    static const size_t strLen =   5 /* 4 знака перед запятой + точка */
                                 + precision;
    std::stringstream tmpStream;
    tmpStream << std::fixed
              << std::setprecision((int)precision)
              << val;
    if(strLen > tmpStream.str().length())
    {
        auto strAppendLen = strLen - tmpStream.str().length();
        tmpStream << std::string(strAppendLen, ' ');
    }
    return tmpStream.str();
}

/******************************************************************************/
std::string AsciiNetGraph::getNameOfScaller()
{
    return _helper.getNameOfScaller(
                            _helper.getCurrentScaleCode()
                            );
}
