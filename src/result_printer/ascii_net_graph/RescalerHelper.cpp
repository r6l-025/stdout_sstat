#include <algorithm>
#include <iostream>

#include "RescalerHelper.h"

/*****************************************************************************/
RescallerHelper::RescallerHelper()
                        : _currentScallerIdx(0)
{
}

/*****************************************************************************/
std::string RescallerHelper::getNameOfScaller(
                                RescallerHelper::Scallers   scalCode
                                )
{
    return _typeNames.at(scalCode);
}

/*****************************************************************************/
bool RescallerHelper::scale(
                          std::vector<double>*  valsArray
                        , double*               newMaxVal
                        )
{
    /* найдем максимальный*/
    auto maxIt = std::max_element(
              valsArray->begin()
            , valsArray->end()
            );
    bool tooSmallNeedRescale = 
           *maxIt < 1.0 
           /* значение текущего уровня масштабирвоания не самое маленькое */
        && _currentScallerIdx != 0;

    auto tmp = (_typeNames.size() - 1);
    bool tooBigNeedRescale = 
           *maxIt > 1000.0 
           /* значение текущего уровня масштабирвоания не самое большое */
        && _currentScallerIdx != tmp;
        
    if( ! (tooSmallNeedRescale || tooBigNeedRescale))
    {
        *newMaxVal = *maxIt;
        return false;
    }

    auto newScaleIds    = calcNewScaleIdx(*maxIt);
    /* ограничим коэфф. масштабирования на случай ошибки */
    if(newScaleIds >= _typeNames.size())
    {
        newScaleIds = _typeNames.size() - 1;
    }
    
    auto newScaleFactor = calcRescaleFactor(newScaleIds);
    _currentScallerIdx  = newScaleIds;

    for(auto& el : *valsArray)
    {
        el *= newScaleFactor;
    }
    /*
     * на коэффициент не умножаем, т.к. работаем с указательем на элемент, а
     * его уже отмасштабирвоали в цикле
     */
    *newMaxVal = *maxIt;
    return true;
}

/*****************************************************************************/
size_t RescallerHelper::calcNewScaleIdx(
                                double  maxValInParrots
                                ) const
{
    double byteVal = maxValInParrots * pow(1024, (double)_currentScallerIdx);
    double testVal;
    size_t scaleIdx = 0;

    for(size_t i = 0; i < _typeNames.size(); ++i)
    {
        testVal = byteVal / 1024;
        if(testVal < 1)
        {
            break;
        }
        scaleIdx = i + 1;
        byteVal = testVal;
    }

    return scaleIdx;
}

/*****************************************************************************/
double RescallerHelper::calcRescaleFactor(
                                size_t  newScaleIdx
                                ) const
{
    if(newScaleIdx > _currentScallerIdx)
    {
        double tmp = (double)newScaleIdx - (double)_currentScallerIdx;
        return 1 / pow(1024, tmp);
    }
    else
    {
        double tmp = (double)_currentScallerIdx - (double)newScaleIdx;
        return pow(1024, tmp);
    }
}

/*****************************************************************************/
double RescallerHelper::getCurrentScaleFactor() const
{
    return 1 / pow(1024, (double)_currentScallerIdx);
}

/*****************************************************************************/
RescallerHelper::Scallers RescallerHelper::getCurrentScaleCode() const
{
    return (RescallerHelper::Scallers)_currentScallerIdx;
}
