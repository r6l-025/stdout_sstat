/*
    SPDX-FileCopyrightText: 2021 Bolshevikov Igor <bolshevikov.igor@gmail.com>
    SPDX-License-Identifier: LGPL-2.1-or-later
*/
#ifndef _RESULT_PRINTER_H_
#define _RESULT_PRINTER_H_

#include <string>

/* local includes */
#include "get_mem_usage.h"
#include "get_disk_usage.h"
#include "get_cpu_usage.h"
#include "get_net_usage.h"
#include "ascii_net_graph/ascii_net_graph.h"

/**
 * @brief Класс отвечающий за формирование форматированного вывода с отчетом
 *        об использовании ресурсов
 *
 * Алгоритм:
 *
 * Для того чтоб колонка с прогресс-барами была выровнена необходимо
 * обеспечить равный размер левой колонки для всех методов вывода. Поэтому
 * расчитаем вручную максимальную длину левой колонки:
 *
 * Cpu usage:
 *      "CPU ##"
 *      - т.е. максимум 6 символов;
 *
 * Mem usage:
 *      "xxx,xxx GB/xxx,xxx GB"
 *      - т.е. максимум 21 символ;
 *
 * Disk usage:
 *      "/dev/sdXXX xxx.xxGB/xxx.xxGB"
 *      - т.е. максимум 28 символов;
 *
 * Поэтому берем максимальную длину строки в левой колонке с запасом -  30
 * символов.
 */
class ResultPrinter
{
public:
    ResultPrinter();
    /**
     * @brief Перечисление типов статистик использования дисковой памяти
     */
    enum class DiskUsageStatTypeT
    {
        /** отображать использование диска */
        UsageIfno,
        /** отображать свободное пространство диска */
        FreeInfo
    };
    
    /**
     * @brief Измерить загруженность CPU, и распечатать результат в строку
     *
     * @note метод формирует итоговую строку, которая будет выводится на экран
     *
     * @param указатель на строку для возврата сформированной строки
     *
     * @retval true, если успешно
     */
    bool printCpuStat(std::string* str);

    /**
     * @brief Измерить загруженность Ram/Swap, и распечатать результат в строку
     *
     * @note метод формирует итоговую строку, которая будет выводится на экран
     *
     * @param указатель на строку для возврата сформированной строки
     *
     * @retval true, если успешно
     */
    bool printRamStat(std::string* str);

    /**
     * @brief Измерить заполненность дисков и распечатать результат в строку
     *
     * @note метод формирует итоговую строку, которая будет выводится на экран
     *
     * @param указатель на строку для возврата сформированной строки
     *
     * @retval true, если успешно
     */
    bool printDiskStat(std::string* str);

    /**
     * @brief Метод запускающий процесс измерения нагрузки на сеть
     */
    bool runNetActivityMeasuring();

    /**
     * @brief Сформировать строки с ascii диаграммой для Rx и Tx каналов
     *
     * @note перед запуском метода небоходимо вызвать метод запускающий процесс
     *       измерения загружености сети
     *
     * @note метод запуска измерения специально вынесен из данного метода т.к.
     *       при работе с Qml была обнаружена странная особенность - при
     *       возврате списка строк обращение к каждому элементу списка запускало
     *       метод получения этого же списка по новой. Что как минимум вызывает
     *       оверхед о ресурсам
     *
     * @param strs - указатель на вектор строк для возврата;
     *
     * @retval true, если формирование строк прошло без ошибок
     */
    void printNetDevStatsRxTx(std::vector<std::string>* strs);
    
    /**
     * @brief Установить тип вывода данных по использованию дискового
     *        пространства
     *
     * @param typeStr строка с именем выбираемого типа
     */
    void setDiskInfoType(const DiskUsageStatTypeT& statType);
    
    /**
     * @brief Сеттер для поля имени выбранно сетевого устройства
     *
     * Сохраняет имя сетевого устройства статистику которого будем отображать
     *
     * @param devName - устанавливаемое имя устройства
     */
    void setUsedNetDevName(const std::string& devName);

    /**
     * @brief Метод получения списка имен сетевых устройств доступных на данной
     * системе
     *
     * @param devicesNames - указатель на вектор для возврата имен обнаруженных 
     *                       сетевых устройств
     */
    void getNetDevicesNames(std::vector<std::string>* devicesNames);
    
private: /* приватные методы */
    /**
     * @brief Получить у системы данные об использовании RAM/SWAP
     *
     * @return true, если метод выполнился успешно
     */
    bool _getMemUsage();
    
    /**
     * @brief Получить у системы данные об использовании дискового пространства
     *
     * @return true, если метод выполнился успешно
     */
    bool _getDiskUsage();
    
    /**
     * @brief Получить у системы данные об использовании CPUs
     *
     * @return true, если метод выполнился успешно
     */
    bool _getCpuUsage();
    
    /**
     * @brief Сформировать левую колонку данных о загруженности CPU
     *
     * @param strings вектор строк для возврата форматированного вывода
     *        (строка на каждый CPU)
     */
    void _makeCpuUsageFirstColumnString(
                                        std::vector<std::string>* strings
                                        );
    
    /**
     * @brief Сформировать правую (с прогресс-баром) колонку данных о
     *        загруженности CPU
     *
     * @param strings вектор строк для возврата форматированного вывода
     *        (строка на каждый CPU)
     */
    void _makeCpuUsageSecondColumnString(
                                        std::vector<std::string>* strings
                                        );
    
    /**
     * @brief Подготовить для вывода набор строк отображающих загруженность CPUs
     *
     * @param string строка в конец которой будет добавлен форматированный вывод
     */
    void _makeCpuUsageString(
                                        std::string* string
                                        );
    
    /**
     * @brief Сформировать левую колонку данных о загруженности дисковой
     *        подсистемы
     *
     * @param strings вектор строк для возврата форматированного вывода
     *        (строка на каждый смонтированный раздел)
     */
    void _makeDiskUsageFirstColumnString(
                                        std::vector<std::string>* strings
                                        );
    
    /**
     * @brief Получить строку со статистикой использования диска в
     *        зависимости от типа выводимой информации
     *
     * @param diskParam - указатель на структуру с парметрами использования
     *                    диска
     *
     * @return сформированная строка
     */
    std::string _getDiskStatString(DiskUsage::DiskInfo* diskParam);
    
    /**
     * @brief Сформировать правую (с прогресс-баром) колонку данных о
     *        загруженности дисковой подсистемы
     *
     * @param strings вектор строк для возврата форматированного вывода
     *        (строка на каждый смонтированный раздел)
     */
    void _makeDiskUsageSecondColumnString(
                                        std::vector<std::string>* strings
                                        );
    std::string _makeDiskUsageSecondColumnPercents(
                                        double percent
                                        );
    /**
     * @brief Подготовить для вывода набор строк отображающих загруженность
     *        дисковой подсистемы
     *
     * @param string строка в конец которой будет добавлен форматированный вывод
     */
    void _makeDiskUsageString(
                                        std::string* string
                                        );
    
    
    /**
     * @brief Сформировать левую колонку данных об использовании памяти RAM/SWAP
     *
     * @param strings вектор строк для возврата форматированного вывода
     * @param param   структура с измеренными параметрами использования RAM
     *                или SWAP
     */
    void _makeMemUsageFirstColumnString(
                                      std::string*                      string
                                    , const MemUsage::MeasuredParams&   param
                                    );
    /**
     * @brief Сформировать правую (с прогресс-баром) колонку данных об
     *        использовании памяти RAM/SWAP
     *
     * @param strings вектор строк для возврата форматированного вывода
     * @param param   структура с измеренными параметрами использования RAM
     *                или SWAP
     */
    void _makeMemUsageSecondColumnString(
                                      std::string*                    string
                                    , const MemUsage::MeasuredParams& param
                                    );
    /**
     * @brief Подготовить для вывода набор строк отображающих загруженность RAM
     *
     * @param string строка в конец которой будет добавлен форматированный вывод
     */
    void _makeRAMUsageString(
                                        std::string* string
                                        );
    /**
     * @brief Подготовить для вывода набор строк отображающих загруженность SWAP
     *
     * @param string строка в конец которой будет добавлен форматированный вывод
     */
    void _makeSWAPUsageString(
                                        std::string* string
                                        );
    
    /**
     * @brief Сформировать строку прогресс-бара отображающую процент заполнения
     *        (от 0 до 100)
     *
     * @param valPercent    - процент;
     * @param lenOfFullBar  - длина 100%й строки (в символах);
     * @param resultStr     - указатель на строку для возврата;
     */
    void _makeProgressBarString(
                                  double        valPercent
                                , size_t        lenOfFullBar
                                , std::string*  resultStr
                                );
    
    /**
     * @brief Конвертирование double в std строку с заданным количеством
     *        символов после запятой
     *
     * @param val       - преобразовываемое значение;
     * @param precision - количество символов после запятой;
     *
     * @return сформированная строка
     */
    std::string _doubleTostring(
                          double    val
                        , int       precision = 2
                        );
private: /* приватные поля */
    /** переменная хранящая измеренные данные по использованию RAM */
    MemUsage::MeasuredParams            _measuredRamUsage;
    /** переменная хранящая измеренные данные по использованию SWAP */
    MemUsage::MeasuredParams            _measuredSwapUsage;
    /**
     * переменная хранящая измеренные данные по использованию дисковой
     * подсистемы
     */
    std::vector<DiskUsage::DiskInfo*>   _measuredDiskUsage;
    /** переменная хранящая измеренные данные по использованию CPU */
    std::vector<double>                 _measuredCpuUsage;
    /** максимальная длина строки в левой колонке форматированного вывода */
    const size_t                        _maxFirstColumnLen = 35;
    /** размер прогресс-бара в ASCII символах */
    const size_t                        _percentStringLen  = 50;
    /** выбранный тип отображаемой статистики по использованию дисков */
    DiskUsageStatTypeT                  _diskStatType;
    /** имя выбранного сетевого устройства */
    std::string                         _selectednNetDevName;
    /** объект класса собирающего статистику по сетевым устройствам */
    NetUsage                            _netUsage;
    /** структура с статистикой по использованию выбранного net устройства */
    NetUsage::DevActivity               _devActivity;
    /** Объект класса ascii графопостроителя сетевой статистики для Rx канала */
    AsciiNetGraph                       _netGraphFormerRx;
    /** Объект класса ascii графопостроителя сетевой статистики для Tx канала */
    AsciiNetGraph                       _netGraphFormerTx;

    std::vector<std::string>            _netGraphPrintedStrings;
};

#endif /* _RESULT_PRINTER_H_ */
